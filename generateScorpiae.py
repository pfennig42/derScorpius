#!/bin/python3
# Generate all the three different Scorpiae Versions

import tempfile
import os
import shutil
import random
import string

source_folder = "./src"
document_class = ["a4", "a5", "a6"]
# document_class = ["a5"]
versions = [
    {
        "type": "normal",
        "name": "Scorpius",
        "mini": False,
        "poc": False,
        "akk": True,
        "chords": True,
        "document_class": "a5",
    },
    {
        "type": "parvus",
        "name": "Scorpius_Parvus",
        "mini": True,
        "poc": False,
        "akk": True,
        "chords": False,
        "document_class": "a6",
    },
    {
        "type": "poc",
        "name": "Scorpius_POC",
        "mini": False,
        "poc": True,
        "akk": True,
        "chords": True,
        "document_class": "a5",
    },
    {
        "type": "compact",
        "name": "Scorpius_Kompakt",
        "mini": False,
        "poc": False,
        "akk": False,
        "chords": True,
        "document_class": "a5",
    },
    {
        "type": "minimal",
        "name": "Scorpius_Minimal",
        "mini": False,
        "poc": False,
        "akk": False,
        "chords": True,
        "document_class": "a5",
    },
]
original_directory = os.getcwd()


def main():
    """
    Iterates through the different page sizes and different types and creates the pdfs in the local folder accordingly.
    """
    for dc in document_class:
        for v in versions:
            r = "".join(random.choices(string.ascii_letters + string.digits, k=5))
            temp_dir = f"./build/{r}"
            generateTemp(source_folder, temp_dir)
            scorpius = os.path.join(temp_dir, "scorpius.tex")
            scorpius_pdf = os.path.join(temp_dir, "scorpius.pdf")
            fillMiniSongs(temp_dir, scorpius)
            modifyScorpius(
                location=scorpius,
                document_class=dc,
                poc=v.get("poc"),
                mini=v.get("mini"),
                akk=v.get("akk"),
                chords=v.get("chords"),
            )
            build(temp_dir)
            if os.path.exists(scorpius_pdf):
                if dc == v.get("document_class"):
                    shutil.copy(
                        scorpius_pdf, f'output/{v.get("name")}.pdf'
                    )  # Move PDF to Folder
                else:
                    shutil.copy(scorpius_pdf, f'output/{v.get("name")}_{dc}.pdf')
            else:
                print(f"{v.get('name')} not generated!")
            shutil.rmtree("./build")


def generateTemp(source_folder: str, destination_folder: str):
    """
    Generates a temporary directory to generate the different versions

    :param str source_folder: The source folder to copy
    :param str destination_folder: The destination folder
    """
    # Create the destination folder if it doesn't exist
    if not os.path.exists(destination_folder):
        os.makedirs(destination_folder)

    # Copy each file and subdirectory in the source folder to the destination folder
    for item in os.listdir(source_folder):
        source_item = os.path.join(source_folder, item)
        destination_item = os.path.join(destination_folder, item)

        if os.path.isdir(source_item):
            # Recursively copy subdirectories
            generateTemp(source_item, destination_item)
        else:
            # Copy files
            shutil.copy2(source_item, destination_item)


def fillMiniSongs(location: str, scorpius: str) -> None:
    """
    Produces a list of all the songs and replaces it in the scorpius.tex

    :param str location: The location of the files
    :param str scorpius: The actual scorpius.tex file
    :return: None
    """

    lis = os.listdir(f"{location}/Lieder")
    lis = sorted(lis)
    formatted_lis = ""
    for i in lis:
        formatted_lis += f"\input{{Lieder/{i}}}" + "\n"
    replaceVariable(scorpius, "% REPLACE_MINI_SONGS", formatted_lis)


def modifyScorpius(
    location: str,
    document_class=str,
    poc: bool = False,
    mini: bool = False,
    akk: bool = True,
    chords: bool = True,
) -> None:
    """
    Replaces the Variables in the scorpius.tex file.

    :param str location: The Location of the scopius.tex
    :param bool poc: True, if chords should be over every line
    :param bool mini: True, if only the short version should appear
    :param bool akk: True, if every line should have chords
    :param bool chords: True, if the minimal chords should be set
    :return: None
    """
    if document_class:
        replaceVariable(
            location,
            "\documentclass[a5paper]{report} % REPLACE_DOCUMENT_CLASS",
            f"\documentclass[{document_class}paper]{{report}}",
        )
    if poc:
        replaceVariable(location, "% REPLACE_PO", "\potrue")
    else:
        replaceVariable(location, "% REPLACE_PO", "\pofalse")
    if mini:
        replaceVariable(location, "% REPLACE_MINI", "\minitrue")
    else:
        replaceVariable(location, "% REPLACE_MINI", "\minifalse")
    if akk:
        replaceVariable(location, "% REPLACE_AKK", "\\akktrue")
    else:
        replaceVariable(location, "% REPLACE_AKK", "\\akkfalse")
    if akk:
        replaceVariable(location, "% REPLACE_CHORDS", "\chordstrue")
    else:
        replaceVariable(location, "% REPLACE_CHORDS", "\chordsfalse")


def replaceVariable(scorpius: str, old: str, new: str) -> None:
    """
    Takes the scorpius.tex in the location and replaces the two variables.

    :param str scorpius: The Location of the scopius.tex
    :param str old: The string to be replaced
    :param str new: The new string
    :return: None
    """
    print("replaced!")
    with open(scorpius, "r") as file:
        lines = file.readlines()

    with open(scorpius, "w") as file:
        for line in lines:
            if old in line:
                line = new + "\n"
            file.write(line)


def build(temp_dir) -> None:
    """
    Runs the default build process after changing the variables.

    :param str temp_dir: The location of the scorpius.tex
    :return: None
    """
    os.chdir(os.path.dirname(temp_dir + "/"))
    os.system(
        "pdflatex -synctex=1 -interaction=nonstopmode -file-line-error scorpius.tex"
    )
    os.system("bibtex scorpius.tex")
    os.system("makeindex  scorpius.idx")
    os.system(
        "pdflatex -synctex=1 -interaction=nonstopmode -file-line-error scorpius.tex"
    )
    os.system(
        "pdflatex -synctex=1 -interaction=nonstopmode -file-line-error scorpius.tex"
    )
    os.chdir(original_directory)


if __name__ == "__main__":
    main()
