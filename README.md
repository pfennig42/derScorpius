# Der SCORPIUS

Willkommen beim Scorpius, die Erweiterung zum "So troll'n wir uns"! Hier findest du viele Lieder, die aktuell im bündischen Liedgut vertreten sind und meiner Meinung nach gerne gesungen werden.

**[Zur aktuellen Version](https://gitlab.com/pfennig42/derScorpius/-/raw/main/output/Scorpius.pdf): https://gitlab.com/pfennig42/derScorpius/-/raw/main/output/Scorpius.pdf!**

Du findest hier zwei Ordner: Zum einen "src", in dem sich alle Quelldateien befinden, sowie "output" wo die kompilierten PDF-Dateien liegen. Du findest in den jeweiligen Ordnern mehr Informationen!

Aus Copyright-Gründen ist hier ein Auszug aus dem Nachwort:

> Ausdrücklicher Dank geht an "Der Keiler," der mich sehr bei der Erstellung der 11. Auflage unterstützt hat.
> Weiterhin danke ich dem \glqq Zugvogel - Deutscher Fahrtenbund e.V.\grqq (Erlaubnis erteilt durch Fotler), Oleg and the Popovs, Till Schönhammer, Manuel Hornauer (Polkageist), Bastian Sandberger(Singekreis Silberburg), Klaus Nies, Dota Kehr, Stimmt So., Pfennig, Moriarty, Werner Helwig (vertreten durch Ursula Prause), Florian Schön, Vico Torriani (vertreten durch Tochter Nicole Kündig-Torriani), Robert Welti (Rökan), Lüül, Heiner Knoch (Bounty), Dimitrie Maron (Schlagsaite), Linnert (Fahrtenbund Vierzig Morgen), Oliver Gies, CUX (Jan Franke), Jürgen Sesselmann (Georg Zierenberg), Kai Deutsch, Stefan Meissel (The Coalminers Beat) und  Eckart Strate für die explizite Druckerlaubnis!
> Bei den anderen Liedern war es mir aufgrund des Alter des Werks, Resonanz o.ä. leider nicht möglich Kontakt zum Autor oder Nachfahren des Autors herzustellen. Vor allem bei hervorragenden Autoren wie Reinhard Mey oder Element of Crime, je vertreten durch Universal Music Group, bitte ich um Nachsicht.
>
> Dieses Liederheft ist gedruckt als Manuskript zum internen Gebrauch.
>
> Hinweise zu den Liedtexten:
> Ich bin bemüht keinerlei Rechtsverletzungen bezüglich der aufgenommenen Lieder zu begehen. Jedoch gehe ich davon aus, dass bei Liedern, die regelmäßig und oft gesungen werden, eine Freigabe des Autors besteht.
> Sollte diesbezüglich irgendeine Art von Fehler passiert sein, bitte ich um eine Nachricht hier auf GitLab. Der strittige Text wird dann umgehend entfernt.
