# Die Liederhefte
Hier findest du alle Liederhefte der aktuellen Version. Möchtest du eine ältere Version anschauen, kannst du auf die History der Dokumente gehen.

Es gibt dabei verschiedene Kategorien, die du jeweils in unterschiedlichen Größen findest. Ist keine Größe im Dateinamen angegeben, handelt es sich um die "Standard"-Größe, also die Größe, die ich normalerweise verwende.
1. Der normale Scorpius (Scorpius.pdf): Das Liederheft in A5, das die Akkorde so über den Zeilen hat, dass es für Gitarrenanfänger:innen am einfachsten ist die Lieder zu lernen, ohne gleichzeitig zu unübersichtlich zu werden. Dies ist die am besten kuratierte Version; hieran orientieren sich die Seitenzahlen.
2. Der kompakte Scorpius (Scorpius_Kompakt.pdf): Hier findest du die Akkorde nur über der ersten Strophe
3. Der minimale Scorpius (Scorpius_minimal.pdf): Es gibt keine Akkorde
4. Der Scorpius POC (per omnes chordas - Scorpius_POC.pdf): Die Akkorde sind über jeder Zeile. Achtung! Dadurch verrutscht das Seitenlayout! Nutze bitte die Liedernummern zur Kompatibilität mit den anderen Heften.
5. Der Scorpius Parvus (Scorpis_Parvus.pdf): Von den Liedern sind jeweils nur die Zeilenanfänge, sowie die Akkorde angegeben. Dieses Heft ist nur für fortgeschrittene Gitarristen und als Gedankenstütze gedacht.